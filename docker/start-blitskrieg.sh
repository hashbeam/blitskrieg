#!/bin/bash

# get params
MYUID=${MYUID:-"1000"}
MYGID=${MYGID:-"1000"}
DOCKER_GID=${DOCKER_GID:-"998"}

# set ownership
echo "Setting file ownership..."
[ "${MYUID}" != "1000" ] && usermod -u "${MYUID}" "${USER}"
[ "${MYGID}" != "1000" ] && groupmod -g "${MYGID}" "${USER}"
groupmod -g ${DOCKER_GID} docker

for DIR in .blitskrieg data; do
  find "${APP_DIR}/${DIR}" \
      \( -not -uid $(id -u ${USER}) -or -gid $(id -g ${USER}) \) \
      -exec chown --silent "${USER}:${USER}" "{}" +
done

# start service
exec gosu ${USER} python3 -m poetry run blitskrieg
