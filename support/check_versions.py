"""Module to check if settings and sample config service versions match."""

import sys
from os import path

import blitskrieg
from blitskrieg import settings


def _get_sample_ver(service):
    """Return current sample configuration version for given service."""
    pkg_path = blitskrieg.__path__[0]
    samp_cfg = path.join(pkg_path, 'share', 'config.sample')
    with open(samp_cfg, 'r', encoding='utf-8') as cfg_file:
        src_lines = cfg_file.readlines()
    for line in src_lines:
        if f'{service}_ref' in line:
            return line.split('=')[-1].strip()
    raise RuntimeError('Could not find requested service version')


def main():
    """Check versions and exit with code 1 if a mismatch is detected.

    Also print found version mismatches to the console.
    """
    mismatch = 0
    for srv in settings.SERVICES:
        sett_ver = getattr(settings, f'{srv.upper()}_REF')
        samp_ver = _get_sample_ver(srv)
        if sett_ver != samp_ver:
            print(f'{srv} version mismatch: {sett_ver} vs {samp_ver}')
            mismatch = 1
    sys.exit(mismatch)


if __name__ == '__main__':
    main()
