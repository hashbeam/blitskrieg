# Changelog
All notable changes to this project will be documented in this file.

The format is based on
[Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to
[Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.2.1]

### Added
- check to only run inside Docker and with the Docker socket available
- check for mismatches in service versions between settings and sample config

### Changed
- bitcoin operations now return an error if there's no running stack
- node operations now return an error if there are no running nodes
- stack operations now return an error if another one is already in progress
- move `init_electrum.sh` under `share/` and copy instead of mounting it
- docker check now instantiates a client instead of looking for the unix socket

## [0.2.0]

### Added
- `Blitskrieg` service: `StackInfo`, `NodeInfo`, `ChannelInfo`
- config file mount when running in development mode
- config section `services` to allow changing the service versions

### Changed
- replaced the `boltlight` package with `boltlight-proto`
- switched to poetry
- minimum python version from 3.7 to 3.9
- click version from 7.x to 8.x
- boltlight version to 2.1.0
- electrs version to 0.9.6
- lnd version to 0.13.4-beta

## [0.1.0]

### Added
- protobuf definition
- gRPC server
- dockerization and compose files
- `unix_helper` utility script
- support for bitcoind
- support for boltlight
- support for c-lightning
- support for eclair
- support for electrs
- support for electrum
- support for lnd
- `Blitskrieg` service: `CreateStack`, `GetInfo`, `RemoveStack`
- `Bitcoin` service: `GetAddress`, `GenTransactions`, `MineBlock`, `Send`
- `Lightning` service: `FundNodes`
- `bli`: a CLI for blitskrieg with bash completion

[Unreleased]: https://gitlab.com/hashbeam/blitskrieg/compare/0.2.1...develop
[0.2.1]: https://gitlab.com/hashbeam/blitskrieg/compare/0.2.0...0.2.1
[0.2.0]: https://gitlab.com/hashbeam/blitskrieg/compare/0.1.0...0.2.0
[0.1.0]: https://gitlab.com/hashbeam/blitskrieg/compare/6d39b47f...0.1.0
